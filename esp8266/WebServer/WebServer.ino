#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

const char* ssid = "Interplanet_NasaSpaceApps";
const char* password = "Vicenza2018";
 
ESP8266WebServer SERVER(80); // instantiate server at port 80 (http port)

// gorge fun: on/off led based on REST Wi-Fi APIs
#define sensor_pin A0


void onGetLEDOn() {
  String page = "{'requested': '/LEDOn'}";
  SERVER.send(200, "application/json", page);
  digitalWrite(LED_BUILTIN, LOW);
}


void onGetLEDOff() {
  String page = "{'requested': '/LEDOff'}";
  SERVER.send(200, "application/json", page);
  digitalWrite(LED_BUILTIN, HIGH);
}


void onGetSensor() {
  int sensor_value = analogRead(sensor_pin);
  String page = String(sensor_value);
  SERVER.send(200, "application/json", page);
  Serial.println(sensor_value);
}


void setup(void){
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(sensor_pin, INPUT);
  
  Serial.begin(115200);
  WiFi.begin(ssid, password); // begin WiFi connection
 
  while (WiFi.status() != WL_CONNECTED) { // Wait for connection
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  SERVER.on("/LEDOn", [](){
    onGetLEDOn();
  });

  SERVER.on("/LEDOff", [](){
    onGetLEDOff();
  });

  SERVER.on("/getSensor", [](){
    onGetSensor();
  });

  SERVER.begin();

  Serial.print("Web server started at ");
  Serial.println(WiFi.localIP());
}

 
void loop(void){
  SERVER.handleClient();
}
