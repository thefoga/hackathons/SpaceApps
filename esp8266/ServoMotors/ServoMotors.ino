/* Sweep
 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Sweep
*/

#include <Servo.h>

bool finallyStopped = false;

Servo myservo;  // create servo object to control a servo
Servo myservo1;
// twelve servo objects can be created on most boards

int toursLeft = 1;
int val = LOW;

void setAngle(int leftAngle, int rightAngle) {
  myservo.write(leftAngle);

  delay(15);
  
  myservo1.write(rightAngle);
}

void reset() {
  setAngle(90, 90);
}

void setup() {
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  myservo1.attach(10);  // attaches the servo on pin 10 to the servo object

  pinMode(7, INPUT);

  Serial.begin(115200);
  delay(1000);
}

void linearMove(int start, int finish) {
  for(int pos = start; pos < finish; pos += 1) {
    int angle = -pos % 360;  // backwards
    
    setAngle(angle, 180 - angle);
  }
}

void spinAround(int angle) {
  for(int pos = 0; pos < angle; pos += 1) {
    setAngle(pos, -pos);
  }

  finallyStopped = true;
}

void loop() {
  val = digitalRead(7);     // read the input pin

  reset();

  if(val > 0 && toursLeft > 0) {
    Serial.println("Moving...");
    linearMove(90, 500);
    toursLeft -= 1;
  }

  if (toursLeft == 0 && !finallyStopped) {
    reset();
    spinAround(90);  // turn around
    linearMove(30, 180);  // come back

    Serial.println("Detaching everything...");
    myservo.detach();
    myservo1.detach();
  }
}
