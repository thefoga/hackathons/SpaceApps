#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
// #include <ArduinoJson.h>

const char* WIFI_SSID = "Interplanet_NasaSpaceApps";
const char* WIFI_PASSWORD = "Vicenza2018";
const String HOSTNAME = "10.0.2.10";
const String ENDPOINT = "getSensor";
const String REQUEST = "http://" + HOSTNAME + "/" + ENDPOINT;

HTTPClient http;

void setup(void){
  Serial.begin(115200);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD); // begin WiFi connection
 
  while (WiFi.status() != WL_CONNECTED) { // Wait for connection
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  http.begin(REQUEST); // Specify request destination

  Serial.print("Successfully connected to ");
  Serial.println(HOSTNAME);
}

int parseRequestPayload(String payload) {
  int value = payload.toInt();
  if (value > 200) {
    return 1;
  } else {
    return 0;
  }
}
 
void loop(void){
  // if (WiFi.status() == WL_CONNECTED) { // Check WiFi connection status
    int httpCode = http.GET(); // Send the request
    // Serial.print("GET ");
    // Serial.print(REQUEST + ": ");
    // Serial.println(httpCode);

    // if (httpCode == 200) { // Check the returning code
      String payload = http.getString(); // Get the request response payload
      int sensor = parseRequestPayload(payload);
      Serial.println(sensor); // Print the response payload
    // }
  // }
}
