#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


int main(int argc, const char * argv[]) {
    Mat input_img = imread("/Users/giorgiofrezza/Desktop/computer vision/laboratorio/lab1/img.png");
    cvtColor(input_img, input_img, CV_BGR2GRAY);
    
    for (int i = 0; i < 1000; i++) {
        Mat modified_img = input_img.clone();
        
        for (int r = 47; r < 208; r++) {
            for (int c = 88; c < 243; c++) {
                int p = rand();
                if (p > 0.1) {
                    uint8_t delta = rand() % 30;
                    modified_img.at<uint8_t>(r, c) = modified_img.at<uint8_t>(r, c) + delta;
                }
            }
        }
        
        String save_path = "/Users/giorgiofrezza/Desktop/computer vision/laboratorio/lab1/foga_stupido/";
        imwrite(save_path + "foga_stupido_" + to_string(i) + ".png", modified_img);
    }
}
