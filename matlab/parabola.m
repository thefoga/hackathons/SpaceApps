function y = parabola(x, a)

y = a * x.^2;
end

