distances = [10; 20; 30; 40; 50];
areas = [1373; 4777; 10913; 19577; 30149];

ft = fittype('parabola(x, a)');
fitting = fit(distances, areas, ft, 'StartPoint', [15])

plot(fitting, distances, areas);