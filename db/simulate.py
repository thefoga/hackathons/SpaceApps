# -*- coding: utf-8 -*-

""" Creates fake MongoDb database with MMOD data """

import matplotlib.image as mpimg
from hal.mongodb.models import DbBrowser
from hal.wrappers.errors import true_false_returns
from matplotlib import pyplot as plt

from config import get_db_name, get_coll_name
from utils import str2img, nice_float


@true_false_returns
def plot_from_db():
    """Plots db images with info

    :return: True iff everything went ok
    """

    db_name = get_db_name()
    coll_name = get_coll_name()
    driver = DbBrowser(db_name)
    documents = driver.get_documents_in_collection(coll_name, with_id=False)
    for doc in documents:
        image = str2img(doc["image"])
        proj_material = doc["proj material"]
        bumper_material = doc["bumper material"]
        dimension = doc["dimension"]
        velocity = doc["velocity"]

        info_text = {
            "projectile material": proj_material,
            "bumper material": bumper_material,
            "dimension": nice_float(dimension) + " mm",
            "velocity": nice_float(velocity) + " km/s"
        }
        info_text = "\n".join([
            key + ": " + value
            for key, value in info_text.items()
        ])

        image = mpimg.imread(image, format='PNG')  # decode as png
        plt.imshow(image, interpolation='nearest')  # plot image
        plt.text(5, 50, info_text)

        plt.show()  # show


def main():
    plot_db = plot_from_db()
    print(plot_db)


if __name__ == '__main__':
    main()
