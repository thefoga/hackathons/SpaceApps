# -*- coding: utf-8 -*-

""" Creates fake MongoDb database with MMOD data """

import os
import random

from hal.files.models.files import Document
from hal.files.models.system import ls_dir
from hal.wrappers.errors import true_false_returns

from db import get_coll
from utils import img2str

here = os.path.abspath(os.path.dirname(__file__))
DATA_FOLDER = os.path.join(here, "data")


def get_random_data():
    """Generates random data about MMOD

    :return: dictionary with velocity, dimension and material
    """

    proj_material = random.choice(["LY12-Al"])
    bumper_material = random.choice(["LY12-Al", "Fe77Si19B4 + LY12-Al"])
    rand_dimension = random.uniform(1, 30)
    rand_velocity = random.uniform(3, 6)
    return {
        "proj material": proj_material,
        "dimension": rand_dimension,
        "velocity": rand_velocity,
        "bumper material": bumper_material
    }


@true_false_returns
def load_to_db(path):
    """Creates db with images from path

    :param path: folder with images and data
    :return: True iff everything went ok
    """

    collection = get_coll()

    # TODO: avoid remove everything, set primary key: name of image
    collection.remove()  # clear collection

    images = [
        file
        for file in ls_dir(path)
        if Document(file).extension == ".png"
    ]  # find images
    data = [
        {
            "image": image,
            "data": image.replace(".png", ".json")  # data file has same name
        } for image in images
    ]
    data = [
        {
            "image": test["image"],
            "data": get_random_data()  # TODO: read data
        } for test in data
    ]
    data = [
        {
            "image": img2str(test["image"]),  # convert images to string
            "proj material": test["data"]["proj material"],  # split data
            "bumper material": test["data"]["bumper material"],
            "dimension": test["data"]["dimension"],
            "velocity": test["data"]["velocity"]
        } for test in data
    ]

    collection.insert_many(data)


def main():
    update_db = load_to_db(DATA_FOLDER)
    print(update_db)


if __name__ == '__main__':
    main()
