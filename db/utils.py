# -*- coding: utf-8 -*-

""" Utils """

import base64
import io
import json


def read_json(path):
    """Reads file and parses it as json
    :param path: file path
    :return: dictionary
    """

    with open(path, "r") as reader:
        return json.load(reader)


def img2str(path):
    """Converts image to strin (base64 encoding)

    :param path: path to image
    :return: image (base64 encoded)
    """

    with open(path, "rb") as reader:
        content = reader.read()
        return base64.b64encode(content)


def str2img(base64_encoded):
    """Decodes base64 encoded image

    :param base64_encoded: image encoded (base64)
    :return: image (binary file)
    """

    image = base64.b64decode(base64_encoded)
    image = io.BytesIO(image)
    return image


def nice_float(val):
    """Pretty format float

    :param val: float value
    :return: string
    """

    return "{:.2f}".format(val)
