from pymongo import MongoClient

from config import get_db_name, get_coll_name


def get_client():
    """Creates a MongoDb client for the app database

    :return: MongoDb client
    """

    return MongoClient()


def get_db():
    """Gets db client of app

    :return: db client
    """

    db_name = get_db_name()
    client = get_client()
    return client[db_name]


def get_coll():
    """Gets default collection of app

    :return: app default collection in database
    """

    collection_name = get_coll_name()
    db = get_db()
    return db[collection_name]
