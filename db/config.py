# -*- coding: utf-8 -*-

""" App configuration """

import os

from hal.wrappers.errors import none_returns

from utils import read_json

here = os.path.abspath(os.path.dirname(__file__))
CONFIG_FILE = os.path.join(here, "config.json")
CONFIG_DATA = read_json(CONFIG_FILE)


@none_returns
def get_config(key):
    """Gets key in configuration file

    :param key: key to get
    :return: configuration value
    """
    return CONFIG_DATA[key]


def get_db_name():
    """Gets name of app database

    :return: database name
    """
    return get_config("db")["name"]


def get_coll_name():
    """Gets name of default collection of app database

    :return: collection name
    """
    return get_config("db")["coll"]
