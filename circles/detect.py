import cv2
import numpy as np

img = cv2.imread('20mm.jpg', 0)
cv2.imshow('image', img)
cv2.waitKey(0)
img = cv2.medianBlur(img, 5)
cv2.imshow('image', img)
cv2.waitKey(0)
cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
cv2.imshow('grey', cimg)
cv2.waitKey(0)
ret, thresh = cv2.threshold(img, 150, 255, cv2.THRESH_BINARY)
cv2.imshow('grey', thresh)
cv2.waitKey(0)

height, width = img.shape[:2]
binary = np.zeros((height, width, 3), np.uint8)
circles = cv2.HoughCircles(thresh, cv2.HOUGH_GRADIENT, 1, 100,
                           param1=30, param2=15, minRadius=1, maxRadius=100)
circles = np.uint16(np.around(circles))
for i in circles[0, :]:
    # draw the outer circle
    cv2.circle(cimg, (i[0], i[1]), i[2], (0, 0, 255), 2)
    cv2.circle(binary, (i[0], i[1]), i[2], (255, 255, 255), -1)
cv2.imshow('detected circles', cimg)
cv2.imshow('binary circles', binary)
cv2.waitKey(0)

counter = 0
for i in range(0, height):
	for j in range(0, width):
		if sum(binary[i, j]) == 3 * 255:
			counter += 1

print (counter)
cv2.destroyAllWindows()
