<div align="center">
	<h1>#YOLO | SPIDER-POD</h1>
	<em>Design by nature Space Apps challenge 2018</em></br>

<a href="https://www.python.org/download/releases/3.6.0/"><img src="https://img.shields.io/badge/Python-3.6-blue.svg"></a></br> <a href="https://saythanks.io/to/sirfoga"><img src="https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg" alt="Say Thanks!" /></a>
</div>


## tl;dr
```
Design an autonomous free-flyer to inspect a spacecraft for damage from Micro-Meteoroid and Orbital Debris (MMOD).
```

## Contributing
[Fork](https://github.com/sirfoga/SpaceApps2018/fork) | Patch | Push | [Pull request](https://github.com/sirfoga/SpaceApps2018/pulls)


## Feedback
Suggestions and improvements are [welcome](https://github.com/sirfoga/SpaceApps2018/issues)!


## Useful links
- [Space Apps Challenge 2018](https://2018.spaceappschallenge.org/)
- [Local event](https://2018.spaceappschallenge.org/locations/vicenza)
- [Challenge](https://2018.spaceappschallenge.org/challenges/can-you-build/design-based-nature-fusion/details)


## Authors

| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") | | [![Frez-g](https://avatars2.githubusercontent.com/u/34913609?s=128&v=4)](https://github.com/Frez-g "Follow @Frez-g on Github") | | |
|---|---|---|---|---|
| [Stefano Fogarollo](https://sirfoga.github.io) | Michele Franzan | Giorgio Frezza | Andrea Michelotti | Bleron Preniqi |


## License
[MIT License](https://opensource.org/licenses/MIT)
